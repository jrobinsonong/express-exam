-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 25, 2021 at 05:33 AM
-- Server version: 8.0.18
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_db`
--
CREATE DATABASE IF NOT EXISTS `blog_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `blog_db`;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'New'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `name`, `content`, `status`) VALUES
(1, 'Sample', 'Blog', 'New'),
(2, 'Sample', 'Blog', 'New'),
(3, 'Sample', 'Blog', 'New'),
(4, 'Sample', 'Blog', 'New'),
(5, 'Sample', 'Blog', 'New'),
(6, 'Sample', 'Blog', 'New'),
(7, 'Sample', 'Blog', 'New'),
(8, 'Sample', 'Blog', 'New'),
(9, 'Sample', 'Blog', 'New'),
(10, 'Sample', 'Blog', 'New'),
(11, 'Sample', 'Blog', 'New'),
(12, 'Sample', 'Blog', 'New'),
(13, 'Sample', 'Blog', 'New'),
(14, 'Sample', 'Blog', 'New'),
(15, 'Sample', 'Blog', 'New'),
(16, 'Sample', 'Blog', 'New'),
(17, 'Robinson', 'New Blog', 'Removed');

-- --------------------------------------------------------

--
-- Table structure for table `blog_images`
--

CREATE TABLE `blog_images` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'New'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_images`
--

INSERT INTO `blog_images` (`id`, `blog_id`, `url`, `status`) VALUES
(1, 14, 'aaaa', 'New'),
(2, 15, 'aaaa', 'New'),
(3, 16, 'aaaa', 'New'),
(4, 16, 'bbbbb', 'New'),
(5, 17, 'aaaa', 'Removed'),
(6, 17, 'bbbbb', 'Removed'),
(7, 17, 'ccccc', 'Removed'),
(8, 17, 'aaaa', 'Removed'),
(9, 17, 'dddd', 'Removed'),
(10, 17, 'ccccc', 'Removed'),
(11, 17, 'aaaa', 'Removed'),
(12, 17, 'dddd', 'Removed'),
(13, 17, 'ccccc', 'Removed'),
(14, 17, 'aaaa', 'Removed'),
(15, 17, 'dddd', 'Removed'),
(16, 17, 'ccccc', 'Removed'),
(17, 17, 'aaaa', 'Removed'),
(18, 17, 'dddd', 'Removed'),
(19, 17, 'ccccc', 'Removed'),
(20, 17, 'xxx', 'Removed'),
(21, 17, 'aaa', 'Removed'),
(22, 16, 'aaaa', 'New'),
(23, 16, 'bbbbb', 'New'),
(24, 16, 'aaaa', 'New'),
(25, 16, 'bbbbb', 'New'),
(26, 16, 'xxxx', 'Removed'),
(27, 16, 'zzzz', 'New');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_images`
--
ALTER TABLE `blog_images`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `blog_images`
--
ALTER TABLE `blog_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
