var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "blog_db"
});

con.connect(err=>{
    err && console.log("Database Connection Error")  
});
const MySQL=sql=>{
  return new Promise(resolve=>{
      con.query(sql,(err,result)=>{
          resolve(result);
      })    
  })
}
module.exports=MySQL;