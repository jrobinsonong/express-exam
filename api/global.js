const fs = require('fs');
const dir = `./api/`;
const folder = fs.readdirSync(dir).filter(data=>data.search(".js")<0);
folder.map(FolderName=>{
    const dir = `./api/${FolderName}`;
    const files = fs.readdirSync(dir);    
    let file;
    files.map(data=>{
        file = require(`./${FolderName}/${data}`);
        module.exports[`${FolderName}`]=module.exports[`${FolderName}`]||{}
        module.exports[`${FolderName}`][`${data.split('.')[0]}`]=file;
    })
})