module.exports={
    exec:(req)=>{
        const MySQL = require('./../../database.js');
        return new Promise(resolve=>{
            const {name,content,images}=req.body;
            const {id}=req.params;
            var sql=`update blog set name="${name}",content="${content}" where id="${id}"`;
            MySQL(sql).then(result=>{
                if(images && images.length>0){
                    sql=`update blog_images set status="Removed" where blog_id="${id}"`
                    MySQL(sql).then(()=>{
                        sql=`insert into blog_images (blog_id,url) values`;
                        let val='';
                        images && images.length>0 && images.map(i=>{
                        val+=val!=''?',':'';
                        val+=`("${id}","${i}")`;  
                        })
                        MySQL(sql+val);
                    })
                } 
                resolve({status:1,data:"Blog Successfully Updated"})
            })         
        })
    },
    promise:true,
    routes:[
        'patch /blog/:id'
    ]
}