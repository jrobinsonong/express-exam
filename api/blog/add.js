module.exports={
    exec:(req)=>{
        const MySQL = require('./../../database.js');
        return new Promise(resolve=>{
            const {name,content,images}=req.body;
            var sql=`insert into blog(name,content) 
                     values("${name}","${content}")`;
            MySQL(sql).then(result=>{
                sql='select id,name,content from blog order by id desc limit 0,1';
                MySQL(sql).then(result=>{
                    const {id}=result[0];
                    sql=`insert into blog_images (blog_id,url) values`;
                    let val='';
                    images.map(i=>{
                      val+=val!=''?',':'';
                      val+=`("${id}","${i}")`;  
                    })
                    MySQL(sql+val);
                    resolve({status:1,data:result[0]});
                })
            })         
        })
    },
    promise:true,
    routes:[
        'post /blog'
    ]
}