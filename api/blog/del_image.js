module.exports={
    exec:(req)=>{
        const MySQL = require('./../../database.js');
        return new Promise(resolve=>{
            const {id}=req.params;
            var sql=`update blog_images set status="Removed" where id="${id}"`;
            MySQL(sql);
            resolve({status:1,data:"Image Successfully Removed"}) 
        })
    },
    promise:true,
    routes:[
        'delete /blog_img/:id'
    ]
}