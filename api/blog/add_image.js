module.exports={
    exec:(req)=>{
        const MySQL = require('./../../database.js');
        return new Promise(resolve=>{
            const {images}=req.body;
            const {id}=req.params
            sql=`insert into blog_images (blog_id,url) values`;
                let val='';
                images.map(i=>{
                val+=val!=''?',':'';
                val+=`("${id}","${i}")`;  
            })
            MySQL(sql+val).then(()=>{
                sql=`select * from blog_images where blog_id="${id}"`;
                MySQL(sql).then(result=>{
                    resolve({status:1,data:result});
                })
            });         
        })
    },
    promise:true,
    routes:[
        'post /blog_img/:id'
    ]
}