module.exports={
    exec:(req)=>{
        const MySQL = require('./../../database.js');
        return new Promise(resolve=>{
            filter='';
            const {id,name}=req.params;
            if(id)  filter+=` and id="${id}"`;
            if(name)filter+=` and name="${name}"`;
            let sql=`select id,name,content from blog where status="New"${filter}`;
            MySQL(sql).then(res=>{
                if(res.length>0){
                    for(let x in res){
                        sql=`select url from blog_images where blog_id="${res[x].id}" and status="New"`;
                        MySQL(sql).then(img=>{
                            img=img.map(data=>{ return data.url })  ||  [];
                            res[x].images=img
                            if(x==res.length-1)resolve({status:1,data:res})
                        })
                    }
                }
                else    resolve({status:1,data:"No Record Found"})
                
            })         
        })
    },
    promise:true,
    routes:[
        'get /blog',
        'get /blog/id/:id',
        'get /blog/name/:name',
    ]
}