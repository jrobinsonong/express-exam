module.exports={
    exec:(req)=>{
        const MySQL = require('./../../database.js');
        return new Promise(resolve=>{
            const {id,blog_id}=req.params;
            let filter='';
            if(id)      filter+=`and id=${id}`;
            if(blog_id) filter+=`and blog_id=${blog_id}`;
            let sql=`select id,url from blog_images
                    where status="New" ${filter}`;
            MySQL(sql).then(res=>{
                resolve({status:1,data:res})
            })         
        })
    },
    promise:true,
    routes:[
        'get /blog_img/blog_id/:blog_id',
        'get /blog_img/id/:id',
        'get /blog_img/'
    ]
}