var express = require('express');
var app = express();
var port=3000;
var multer  = require('multer');
const bodyParser = require('body-parser');
const global = require('./api/global.js');
console.log("==========================");

//|=====================| Upload File Setting |===============|//
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './assets/images')
    },
    filename: function (req, file, cb) {
        console.log(file);
        const {fieldname,originalname}=file;
        cb(null, `${fieldname}-${Date.now()}.${originalname.split('.')[1]}`);
    }
})
var upload = multer({storage})
app.post('/upload',upload.array('file', 10), function (req, res, next) {
    console.log(req.files)
    let data=[];
    data=req.files.map(img=>{
        const {filename,originalname}=img;
        return  `localhost:3000/images/${filename}`
    })
    res.send({status:1,msg:"Successfully Upload",data,info:req.body})
});
//|======================| Public Folder Setting |============|//
app.use(express.static('assets'))
//|===========================================================|//
var forms = multer();
app.use(bodyParser.json());
app.use(forms.array()); 
Object.keys(global).map(index=>{
    const api=global[index];
    Object.keys(api).map(func=>{
        const {routes,exec,promise}=api[func];
        routes && routes.map(data=>{
            const route=data.split(' ');
            app[route[0]](route[1],(req,res)=>{
                if(!promise){
                    res.send(exec(req))
                }
                else{
                    exec(req).then(result=>{
                        res.send(result)
                    });
                }
            })
        })
    })
})
//|===========================================================|//
app.listen(port);
console.log(`Server is Running at port ${port}`)